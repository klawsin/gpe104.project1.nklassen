﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public GameObject theObject; //for grabbing our own game object

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //When "P" is pressed, disable/enable transform component
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (GetComponent<Movement>().enabled == true)
            {
                GetComponent<Movement>().enabled = false;
            }
            else
            {
                GetComponent<Movement>().enabled = true;
            }
        }

        //When "Q" is pressed, set GameObject to inactive
        if (Input.GetKeyDown(KeyCode.Q))
        {
            theObject.SetActive(false);
        }
    }
}
