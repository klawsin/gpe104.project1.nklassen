﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    //Declare variables
    private Transform tf; //will hold Transform component
    public float speed = 0.2f; //change speed willingly
    private bool shiftReset = true; //for moving one unit at a time with shift

    // Start is called before the first frame update
    void Start()
    {
        //get transform component
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //if shift is being held, move only one unit at a time
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            //if reset is true then allow to move
            if (shiftReset == true)
            {
                //left
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    tf.position = tf.position + (Vector3.left);
                    //set reset to false to stop from moving more than one unit
                    shiftReset = false;
                }
                //right
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    tf.position = tf.position + (Vector3.right);
                    //set reset to false to stop from moving more than one unit
                    shiftReset = false;
                }
                //up
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    tf.position = tf.position + (Vector3.up);
                    //set reset to false to stop from moving more than one unit
                    shiftReset = false;
                }
                //down
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    tf.position = tf.position + (Vector3.down);
                    //set reset to false to stop from moving more than one unit
                    shiftReset = false;
                }
            }
        }
        else //press arrow to move normally while shift is not pressed
        {
            //left
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                tf.position = tf.position + (Vector3.left * speed);
            }
            //right
            if (Input.GetKey(KeyCode.RightArrow))
            {
                tf.position = tf.position + (Vector3.right * speed);
            }
            //up
            if (Input.GetKey(KeyCode.UpArrow))
            {
                tf.position = tf.position + (Vector3.up * speed);
            }
            //down
            if (Input.GetKey(KeyCode.DownArrow))
            {
                tf.position = tf.position + (Vector3.down * speed);
            }
        }

        //letting go of arrow resets shiftReset
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
        {
            shiftReset = true;
        }

        //Pressing space bar resets position to center
        if (Input.GetKeyDown(KeyCode.Space))
        {
            tf.position = Vector3.zero;
        }
    }
}
